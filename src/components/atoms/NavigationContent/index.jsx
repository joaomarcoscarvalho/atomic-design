const NavigationContent = ({ icon, buttonName }) => {
  return (
    <span>
      {icon}
      {buttonName}
    </span>
  );
};

export default NavigationContent;
