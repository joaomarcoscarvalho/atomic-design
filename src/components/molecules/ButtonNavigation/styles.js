import styled from "styled-components";

export const Button = styled.button`
  background-color: transparent;
  border: none;
  margin-top: 5%;
  padding: 10%;
  outline: none;
  &:hover {
    background-color: blue;
  }
`;
