import NavigationContent from "../../atoms/NavigationContent";
import { Button } from "./styles.js";
const ButtonNavigation = ({ children, handleClick }) => {
  return (
    <Button onClick={handleClick}>
      <NavigationContent buttonName={children} />
    </Button>
  );
};

export default ButtonNavigation;
