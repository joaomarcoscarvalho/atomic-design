import { MenuBar } from "./styles.js";
import ButtonNavigation from "../../molecules/ButtonNavigation";
import Title from "../../atoms/Title";
import { AiOutlineShoppingCart, AiOutlineUnorderedList } from "react-icons/ai";

import { useHistory } from "react-router-dom";

const MainMenu = () => {
  const history = useHistory();

  const register = () => history.push("/register");

  const list = () => history.push("/productlist");

  return (
    <MenuBar>
      <Title>Shop+</Title>
      <ButtonNavigation handleClick={register}>
        <AiOutlineShoppingCart />
        Cadastrar Produto
      </ButtonNavigation>
      <ButtonNavigation handleClick={list}>
        <AiOutlineUnorderedList />
        Listar Produtos
      </ButtonNavigation>
    </MenuBar>
  );
};

export default MainMenu;
