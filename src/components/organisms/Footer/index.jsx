import Text from "../../atoms/Text";

import { Container } from "./styles.js";
const Footer = () => {
  return (
    <Container>
      <Text>Desenvolvido por João</Text>
    </Container>
  );
};

export default Footer;
