import styled from "styled-components";

export const Container = styled.div`
  position: absolute;
  width: 80vw;
  height: 10vh;
  background-color: blue;
  bottom: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
`;
