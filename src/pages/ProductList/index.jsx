import MainTemplate from "../../Templates/MainTemplate";

const ProductList = () => {
  return (
    <MainTemplate>
      <h1>Lista de produtos</h1>
    </MainTemplate>
  );
};

export default ProductList;
