import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Register from "./pages/Register";
import ProductList from "./pages/ProductList";
const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/register" component={Register} />
      <Route path="/productlist" component={ProductList} />
    </Switch>
  );
};

export default Routes;
