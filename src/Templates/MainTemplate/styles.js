import styled from "styled-components";

export const HomeContent = styled.div`
  display: flex;
  height: 100vh;
`;

export const MainContent = styled.div`
  border: 1px solid red;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 80vw;
`;
