import MainMenu from "../../components/organisms/MainMenu";
import Footer from "../../components/organisms/Footer";
import { HomeContent, MainContent } from "./styles.js";

const MainTemplate = ({ children }) => {
  return (
    <HomeContent>
      <MainMenu />
      <MainContent>{children}</MainContent>
      <Footer />
    </HomeContent>
  );
};

export default MainTemplate;
